from django.apps import AppConfig


class HotelManagmentConfig(AppConfig):
    name = 'hotel_managment'
