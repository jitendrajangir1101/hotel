from django.contrib import admin
from .models import *

admin.site.register(Room)
admin.site.register(Reservation)
admin.site.register(Chat)
admin.site.register(Feedback)
admin.site.register(avail)

# Register your models here.
