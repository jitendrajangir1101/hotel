from django.urls import path 
from .import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
   path('index/', views.index , name='index'),
   path('overview/', views.overview , name='overview'),
   path('login/', views.login , name='login'),
   path('logout/', views.logout , name='logout'),
   path('payment/', views.payment , name='payment'),
   path('video/', views.video , name='video'),
   path('book/', views.book , name='book' ),
   path('welcome/', views.welcome , name='welcome' ),
   path('feedback/', views.feedback , name='feedback' ),
   path('chat/', views.chat , name='chat' ),
   path('cuslogin/',views.cuslogin , name='cuslogin'),
   path('cus_signup/' , views.cus_signup , name='cus_signup'),
   path('admin_login/' , views.admin_login , name='admin_login'),
   path('adminlogin_home/',views.adminlogin_home , name='adminlogin_home'),
   path('get_room_no/',views.get_room_no , name='get_room_no'),
   
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
