from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.models import User
class Room(models.Model):
    Rno = models.AutoField(primary_key=True)
    ch = (("1" , "A/C") ,("2", "Non AC") )
    RType=models.CharField(max_length=15 , choices=ch)
    cc = (("1" , "Single") ,("2", "Double") ,("3" , "Three") )
    Bedno=models.CharField(max_length=15 , choices=cc)
    objects = models.manager
    def __str__(self):
        return self.RType 
class Reservation(models.Model):
    """Models for reservations"""
    r_id = models.AutoField(primary_key=True)
    Cname = models.CharField(max_length = 100)
    Email= models.CharField(max_length = 100)
    Mobile= models.BigIntegerField()
    Cin = models.DateTimeField()
    Cout = models.DateTimeField()
    RType=models.CharField(max_length=100)
    Bedno=models.CharField(max_length=15)
    cust_id=models.ForeignKey(User , on_delete=models.CASCADE  )
    Room_no=models.ForeignKey(Room , on_delete=models.CASCADE )
    objects = models.manager
    def __str__(self):
        return (self.Cname)
class avail(models.Model):
    Room_no = models.ForeignKey(Room , on_delete=models.CASCADE ,blank=True)
    availability = models.BooleanField(default=0)
    objects = models.manager
class Chat(models.Model):
    Cid = models.AutoField(Room , primary_key=True )
    cust_id=models.ForeignKey(User , on_delete=models.CASCADE ,blank=True , default=None) 
    msg=models.TextField()
    objects = models.manager
class Feedback(models.Model):
    cust_id=models.ForeignKey(User , on_delete=models.CASCADE ,blank=True ,default=None)
    Fid = models.AutoField(Room ,  primary_key=True)
    msg=models.TextField()
    objects = models.manager


# Create your models here.
