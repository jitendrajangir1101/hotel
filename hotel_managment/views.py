from django.shortcuts import render, redirect

from django.contrib.auth.models import User, auth
from django.contrib import messages
from .models import *
from django.http import HttpResponse


def index(request):
    print("index page request")
    return render(request, "index.html")


def login(request):
    print("login page request")
    return render(request, "login.html")


def payment(request):
    print("payment page request")
    return render(request, "payment.html")


def video(request):
    print("video page request")
    return render(request, "video.html")


def overview(request):
    print("overview page request")
    return render(request, "overview.html")


def admin_login(request):
    print("overview page request")
    return render(request, "admin_login.html")


def book(request):
    print("overview page request")
    return render(request, "book.html")


def logout(request):
    auth.logout(request)
    return redirect('index')


def adminlogin_home(request):
    if request.method == 'POST':
        user = auth.authenticate(
            username=request.POST['username'], password=request.POST['password'])
        if user.is_superuser == True:
            if user is not None:
                auth.login(request, user)
                return render(request, "admin_home.html")
            else:
                return render(request, 'admin_login.html', {'error': "invalid login credentials"})
        else:
            return HttpResponse("invalid login credentials")
    else:
        return render(request, "admin_login.html")


def welcome(request):

    if request.method == "POST":
        nm = request.POST["Cname"]
        em = request.POST["Email"]
        mm = request.POST["Mobile"]
        Cin = request.POST["Cin"]
        cout = request.POST["Cout"]
        rt = request.POST["RType"]
        bd = request.POST["Bedno"]
        rn=request.POST["Room_no"]
        print(rn)
        Room_no=Room.objects.get(Rno=rn)
        print(Room_no)
        cid=request.POST["cust_id"]
        cust_id=User.objects.get(id=cid)
        if Reservation.objects.filter(Room_no=Room_no).exists() and Reservation.objects.filter(Cin=Cin).exists():
            print("room already taken")
            return render(request, "cus_home.html")
        else:
        
            Reservation.objects.create(Cname=nm, Email=em, Mobile=mm, Cin=Cin, Cout=cout,RType=rt, Bedno=bd,Room_no=Room_no,cust_id=cust_id)
            print("booking request")
            return render(request, "welcome.html")
    else:
        return render(request, "cus_home.html")


def feedback(request):
    if request.method == "POST":
        ms = request.POST["msg"]
        data = Feedback(msg=ms)
        print(data)
        data.save()
        print("chat request")
        return render(request, "feedback.html")
    else:
        return render(request, "book.html")


def chat(request):
    if request.method == "POST":
        ms = request.POST["msg"]
        data = Chat(msg=ms)
        print(data)
        data.save()
        print("chat request")
        return render(request, "chatrcv.html")
    else:
        return render(request, "book.html")


def cuslogin(request):
    if request.method == 'POST':
        user = auth.authenticate(
            username=request.POST['username'], password=request.POST['password'])
        if user.is_superuser == False:
            if user is not None:
                auth.login(request, user)
                return render(request, "cus_home.html")
            else:
                return render(request, 'login.html', {'error': "invalid login credentials"})
        else:
            return HttpResponse("invalid login credentials")
    else:
        return render(request, "login.html")


def cus_signup(request):
    if request.method == "POST":
        email = request.POST["email"]
        first_name = request.POST["first_name"]
        last_name = request.POST["last_name"]
        username = request.POST["username"]
        password = request.POST["password"]
        passwordagain = request.POST["passwordagain"]
        if password == passwordagain:
            if User.objects.filter(username=username).exists():
                messages.info(request, 'username already exists')
                return render(request, "login.html")
            elif User.objects.filter(email=email).exists():
                messages.info(request, 'email already exists')
                return render(request, "login.html")
            else:
                user = User.objects.create_user(
                    email=email, first_name=first_name, last_name=last_name, username=username, password=password)
                print(user)
                user.save()
                print("customer added")
                return render(request, "cus_home.html")
        else:
            messages.info(request, 'password not matching try again')
            return render(request, "login.html")
    else:
        return render(request, 'login.html')


def get_room_no(request):
    if request.method == "GET":
        from django.http import JsonResponse
        # .filter(RType=request.GET['rtype']).filter(Bedno=request.GET['badno'])
        query = Room.objects.filter(
            RType=request.GET['rtype'], Bedno=request.GET['badno'])
        data = []
        for q in query:
            data.append(q.Rno)
        return JsonResponse({"status": 0, "msg": "Success", 'roomno': data})
    else:
        return JsonResponse({"status": 0, "msg": "Invalid request"})

# Create your views here.
